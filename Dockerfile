FROM node:alpine as builder

WORKDIR /app

ADD package.json .
ADD package-lock.json .
RUN npm install

ARG BACKEND_URL
ENV VITE_BACKEND_URL=$BACKEND_URL

ADD ./ /app/
RUN npm run build

# run lightweighted container with dist only
FROM nginx
COPY --from=builder /app/dist/ /usr/share/nginx/html