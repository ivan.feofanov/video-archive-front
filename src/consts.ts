export const API_VER = 1
export const SERVER_URL = import.meta.env.VITE_BACKEND_URL ? import.meta.env.VITE_BACKEND_URL : 'http://localhost:8000'

export const TOKEN_KEY = 'authToken'
